From 4c1ed1a6c74b57ea811669b47161241551cbcf56 Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?Nelson=20Ben=C3=ADtez=20Le=C3=B3n?=
 <nbenitezl+gnome@gmail.com>
Date: Sat, 21 Oct 2017 17:23:04 +0500
Subject: [PATCH] gtkfilechooserwidget: add common shortcuts for file
 operations

Add the same shortcuts used by Nautilus for the same operations,
which are:
  "New folder" -> Ctrl + Shift + N
  "Rename" -> F2
  "Delete" -> Shift + Delete

As a nice feature for advanced users, this shortcuts will also
work for the ACTION_OPEN and ACTION_SELECT_FOLDER modes, which hide
the UI elements for said operations, but will now be possible if
invoked through its shortcut/keybinding.

https://bugzilla.gnome.org/show_bug.cgi?id=789285
---
 gtk/gtkfilechooserwidget.c | 142 +++++++++++++++++++++++++++++++++++++++++++++
 1 file changed, 142 insertions(+)

diff --git a/gtk/gtkfilechooserwidget.c b/gtk/gtkfilechooserwidget.c
index 09a119049c..b31f5e0061 100644
--- a/gtk/gtkfilechooserwidget.c
+++ b/gtk/gtkfilechooserwidget.c
@@ -377,9 +377,12 @@ enum {
   DOWN_FOLDER,
   HOME_FOLDER,
   DESKTOP_FOLDER,
+  NEW_FOLDER,
   QUICK_BOOKMARK,
   LOCATION_TOGGLE_POPUP,
   SHOW_HIDDEN,
+  RENAME_SHORTCUT,
+  DELETE_SHORTCUT,
   SEARCH_SHORTCUT,
   RECENT_SHORTCUT,
   PLACES_SHORTCUT,
@@ -8068,6 +8071,70 @@ show_hidden_handler (GtkFileChooserWidget *impl)
   g_object_set (impl, "show-hidden", !priv->show_hidden, NULL);
 }
 
+static void
+delete_shortcut_handler (GtkFileChooserWidget *impl)
+{
+  GActionGroup *actions;
+  GAction *action;
+  GtkFileChooserWidgetPrivate *priv;
+
+  priv = impl->priv;
+
+  if (priv->operation_mode != OPERATION_MODE_BROWSE)
+    return;
+
+  /* updates action sensitiveness based on file selection */
+  file_list_build_popover (impl);
+  check_file_list_popover_sensitivity (impl);
+
+  actions = gtk_widget_get_action_group (priv->browse_files_tree_view, "item");
+
+  action = g_action_map_lookup_action (G_ACTION_MAP (actions), "delete");
+
+  if (g_action_get_enabled (action))
+    g_action_activate (action, NULL);
+}
+
+static void
+new_folder_handler (GtkFileChooserWidget *impl)
+{
+  GtkFileChooserWidgetPrivate *priv;
+
+  priv = impl->priv;
+
+  if (!priv->create_folders || priv->operation_mode != OPERATION_MODE_BROWSE)
+    return;
+
+  if (!gtk_widget_get_visible (priv->browse_new_folder_button))
+    gtk_widget_set_visible (priv->browse_new_folder_button, TRUE);
+
+  gtk_button_clicked (GTK_BUTTON (priv->browse_new_folder_button));
+}
+
+static void
+rename_shortcut_handler (GtkFileChooserWidget *impl)
+{
+  GActionGroup *actions;
+  GAction *action;
+  GtkFileChooserWidgetPrivate *priv;
+
+  priv = impl->priv;
+
+  if (priv->operation_mode != OPERATION_MODE_BROWSE)
+    return;
+
+  /* updates action sensitiveness based on file selection */
+  file_list_build_popover (impl);
+  check_file_list_popover_sensitivity (impl);
+
+  actions = gtk_widget_get_action_group (priv->browse_files_tree_view, "item");
+
+  action = g_action_map_lookup_action (G_ACTION_MAP (actions), "rename");
+
+  if (g_action_get_enabled (action))
+    g_action_activate (action, NULL);
+}
+
 static void
 add_normal_and_shifted_binding (GtkBindingSet   *binding_set,
                                 guint            keyval,
@@ -8268,6 +8335,69 @@ gtk_file_chooser_widget_class_init (GtkFileChooserWidgetClass *class)
                                 NULL,
                                 G_TYPE_NONE, 0);
 
+  /**
+   * GtkFileChooserWidget::new-folder:
+   * @widget: the object which received the signal
+   *
+   * The ::new-folder signal is a [keybinding signal][GtkBindingSignal]
+   * which gets emitted when the user asks for it.
+   *
+   * This is used to make the file chooser show the 'create new folder'
+   * dialog.
+   *
+   * The default binding for this signal is `Ctrl + Shift + N`.
+   */
+  signals[NEW_FOLDER] =
+    g_signal_new_class_handler (I_("new-folder"),
+                                G_OBJECT_CLASS_TYPE (class),
+                                G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
+                                G_CALLBACK (new_folder_handler),
+                                NULL, NULL,
+                                NULL,
+                                G_TYPE_NONE, 0);
+
+  /**
+   * GtkFileChooserWidget::rename-shortcut:
+   * @widget: the object which received the signal
+   *
+   * The ::rename-shortcut signal is a [keybinding signal][GtkBindingSignal]
+   * which gets emitted when the user asks for it.
+   *
+   * This is used to make the file chooser show the dialog to rename a
+   * file or folder.
+   *
+   * The default binding for this signal is `F2`.
+   */
+  signals[RENAME_SHORTCUT] =
+    g_signal_new_class_handler (I_("rename-shortcut"),
+                                G_OBJECT_CLASS_TYPE (class),
+                                G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
+                                G_CALLBACK (rename_shortcut_handler),
+                                NULL, NULL,
+                                NULL,
+                                G_TYPE_NONE, 0);
+
+  /**
+   * GtkFileChooserWidget::delete-shortcut:
+   * @widget: the object which received the signal
+   *
+   * The ::delete-shortcut signal is a [keybinding signal][GtkBindingSignal]
+   * which gets emitted when the user asks for it.
+   *
+   * This is used to make the file chooser show the dialog to delete
+   * permanently a file or folder.
+   *
+   * The default binding for this signal is `Shift + Delete`.
+   */
+  signals[DELETE_SHORTCUT] =
+    g_signal_new_class_handler (I_("delete-shortcut"),
+                                G_OBJECT_CLASS_TYPE (class),
+                                G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
+                                G_CALLBACK (delete_shortcut_handler),
+                                NULL, NULL,
+                                NULL,
+                                G_TYPE_NONE, 0);
+
   /**
    * GtkFileChooserWidget::quick-bookmark:
    * @widget: the object which received the signal
@@ -8414,6 +8544,18 @@ gtk_file_chooser_widget_class_init (GtkFileChooserWidgetClass *class)
                                 GDK_KEY_d, GDK_MOD1_MASK,
                                 "desktop-folder",
                                 0);
+  gtk_binding_entry_add_signal (binding_set,
+                                GDK_KEY_n, GDK_CONTROL_MASK | GDK_SHIFT_MASK,
+                                "new-folder",
+                                0);
+  gtk_binding_entry_add_signal (binding_set,
+                                GDK_KEY_F2, 0,
+                                "rename-shortcut",
+                                0);
+  gtk_binding_entry_add_signal (binding_set,
+                                GDK_KEY_Delete, GDK_SHIFT_MASK,
+                                "delete-shortcut",
+                                0);
   gtk_binding_entry_add_signal (binding_set,
                                 GDK_KEY_h, GDK_CONTROL_MASK,
                                 "show-hidden",
-- 
2.13.6

